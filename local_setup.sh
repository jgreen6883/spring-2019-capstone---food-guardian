## setup script for Food Guardian. The user running this
## needs sudo priviledges to add the boot script to root's
## crontab (to run the program on boot). The main executable
## also needs root privs because it has a library that needs
## direct hardware access. 


## This creates a convenience link
## for the user to copy images to and sets a script to run
## on boot up.


## link desktop folder to images
ln -s $PWD/images $HOME/Desktop/pet_pics


## run bootup script
line="@reboot $PWD/scripts/on_boot.sh"
(sudo crontab -u root -l; echo "$line" ) | sudo crontab -u root -

