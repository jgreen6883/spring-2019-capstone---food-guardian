#!/usr/bin/env python
# coding: utf-8


"""
image_comparison.py - Compares images and returns a match / not-match
    value.
"""


## makes python 2 division and print act like python 3,
## here for compatibility reasons
from __future__ import division, print_function
import cv2
import numpy as np
import log as lg


class ImageComparison(object):
    """
    This class uses histograms to compare pixels between images to 
    determine how similar they are.
    """
    
    ## bins for histograms
    HIST_BINS = [15, 10, 25]
    ## four methods opencv provides to compare histograms
    METHODS = (
        ("Correlation", cv2.HISTCMP_CORREL),
        ("Chi-Squared", cv2.HISTCMP_CHISQR),
        ("Intersection", cv2.HISTCMP_INTERSECT),
        ("Hellinger", cv2.HISTCMP_BHATTACHARYYA),
    )
    DEFAULT_WEIGHTS = {
        "Correlation" : 1.0,
        "Chi-Squared" : -0.5,
        "Intersection" : 1.0,
        "Hellinger" : 0.5,
    }


    def __init__(self, images, weights=False):
        """
        Constructor for the ImageComparison class.
        
        A set of images is needed to have something to compare with. 
        Optional weight values for the comparison methods can be passed 
        in to use instead of the defaults.
                
        param: images, a set of images to compare with
        param: weights, weights for match methods
        """
        
        self.hists = [self._make_histogram(img) for img in images]
        self.results = {
            "Correlation" : 0,
            "Chi-Squared" : 0,
            "Intersection" : 0,
            "Hellinger" : 0,
        }
        if weights:
            self.set_weights(weights)
        else:
            self.set_weights(self.DEFAULT_WEIGHTS)
        self.logger = lg.get_interface()


    def set_weights(self, w):
        """
        Provides a way to set the match weights.
        
        param: w, python dictionary for method weights
        """
        
        self.weights = w


    def compare_image(self, image):
        """
        Performs the histogram comparison of a given image to the histograms
        from the saved images.
        
        param: image, an image to compare to stored images
        returns: float, a value representing how well the image matches 
            the saved images.
        """
        
        cam_hist = self._make_histogram(image)
        for method_name, method in self.METHODS:
            d = 0.0
            for hist in self.hists:
                dist = cv2.compareHist(cam_hist, hist, method)
                d += dist
                print(method_name, "dist:", dist)
            ## save the mean of the comparisons to each saved image
            self.results[method_name] = d / len(self.hists)
            print(method_name, "D MEAN:", d / len(self.hists))
        print("RAW VALUES:", self.results)
        ## scale the output scores from 0 to 1
        for method_name, _ in self.results.items():
            if method_name == "Chi-Squared":
                self.results[method_name] = self._scale_chi_squared(
                    self.results[method_name]
                )
            if method_name == "Hellinger":
                self.results[method_name] = self._scale_hellinger(
                    self.results[method_name]
                )
            if method_name == "Intersection":
                self.results[method_name] = self._scale_intersect(
                    self.results[method_name]
                )
        print("SCALED VALUES:", self.results)
        mval = 0
        ## apply weights to the results and return sums
        for method_name, method_value in self.results.items():
            r = method_value * self.weights[method_name]
            print(method_name, r)
            mval += r
        print("MVAL:", mval)
        self.logger.log_message("Comparison value: %f" % mval)
        return mval


    def _make_histogram(self, image):
        """
        Creates a histogram from a given image. This image has already 
        been read via opencv and is represented as a numpy array of pixel
        values.
        
        param: image, array of pixel info from the image
        returns: ndarray, array of pixel info from the image
        """
        
        color_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask = self._get_mask(image)
        hist = cv2.calcHist(
            [color_image], 
            [0, 1, 2], 
            mask, 
            self.HIST_BINS, 
            [0, 180, 0, 256, 0, 256]
        )
        return cv2.normalize(hist, hist).flatten()


    def _get_mask(self, image):
        """
        Creates an elliptical mask centered on the given image. This 
        mask is used to only compare the pixels contained in the ellipse 
        instead of all the pixels in the image.
        
        param: image, array of pixel info of a color image
        returns: ndarray, array of pixel info of the mask
        """
        
        image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        h, w = image.shape[:2]
        ## center of image
        cx, cy = int(w * 0.5), int(h * 0.5)
        ## range for x and y axis from center
        xr, yr = int(w * 0.75) // 2, int(h * 0.75) // 2
        ellipse_mask = np.zeros(image.shape[:2], dtype = "uint8")
        ## set array values of mask
        cv2.ellipse(
            ellipse_mask, 
            (cx, cy), 
            (xr, yr), 
            0, 0, 360, 255, -1
        )
        return ellipse_mask


    def _scale_chi_squared(self, val):
        """
        Returns a scaled value from 0 to 1 of the given input value.
        
        This is a piecemeal linear function. Smaller values get a higher
        score up to 1500. The score is more level up to 6000. Then the 
        score climbs faster to the max at 14000. 
        
        This gives extremely low or high input a high output while leaving 
        a range in the center where the change isn't drastic from the start 
        to the end. 
        
        param: val, value scaled from 0 with lower numbers meaning a 
            closer match
        returns: float, val scaled from 0 to 1 with higher numbers meaning
            a closer match
        """
        if val <= 1500:
            return -val * 0.5 / 1500.0 + 1
        elif val <= 6000:
            return val * 0.2 / 4500 + 0.14
        elif val <= 14000:
            return val * 0.6 / 8000 - 0.05
        else:
            return 1


    def _scale_hellinger(self, val):
        """
        Returns a scaled value from 0 to 1 of the given input value.
        
        param: val, value scaled from 1 to 0 with lower numbers meaning
            a closer match
        returns: float, val scaled from 0 to 1 with higher numbers meaning
            a closer match
        """
        return -val + 1


    def _scale_intersect(self, val):
        """
        Returns a scaled value from 0 to 1 of the given value.
        
        param: val, value scaled from 0 with higher numbers meaning a 
            closer match
        returns: float, val scaled from 0 to 1 with higher numbers meaning
            a closer match
        """
        if val < 4:
            return val / 4.0
        else:
            return 1

