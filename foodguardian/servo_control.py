#!/usr/bin/env python
# coding: utf-8


"""
servo_control.py - A class that controls a servo motor.
"""


## makes python 2 division and print act like python 3,
## here for compatibility reasons
from __future__ import division, print_function
import wiringpi
import log as lg
import time


class ServoControl(object):
    """
    This class will control a servo motor. The servo has a 180 degree 
    range of travel and uses a PWM (pulse width modulation) signal
    to decide the correct position. 
    """
    
    ## used to pause the servo during movement so it's not
    ## always moving at full speed
    MIN_DELAY = 0.005
    MAX_DELAY = 0.05
    INIT_DELAY = 0.01
    ## slowest and fastest speed the servo will move
    MIN_SPEED = 1
    MAX_SPEED = 10
    ## range of travel in degrees
    MIN_ANGLE = 0
    MAX_ANGLE = 180
    ## these setting affect the pulse width of the signal
    ## used to move the servo, you prolly don't want to change 
    ## these unless you're really sure about it
    PWM_CLOCK = 192
    PWM_RANGE = 2000
    ## pulse width range for 50Hz
    MIN_PULSE = 50
    MAX_PULSE = 250
    ## threshold for comparing floats
    THRESHOLD = 0.0001


    def __init__(self, gpio_pin):
        """
        Constructor for the ServoControl class.
        
        The only required parameter is which gpio pin the pwm signal is 
        coming from. PWM0 on the pi is physical pin 12, but gpio is 
        numbered different so that one would be gpio 18.
        
        param: gpio_pin, the pwm output pin on the pi
        """
        
        self.pwm_pin = gpio_pin
        self.delay = self.INIT_DELAY
        self.curr_pw = self.MIN_PULSE
        ## use gpio numbers instead of physical pin numbers
        wiringpi.wiringPiSetupGpio()
        ## setup the pwm output pin
        wiringpi.pinMode(
            self.pwm_pin, 
            wiringpi.GPIO.PWM_OUTPUT
        )
        ## set the PWM mode to milliseconds stype
        wiringpi.pwmSetMode(wiringpi.GPIO.PWM_MODE_MS)
        ## divide down clock needed by servo
        wiringpi.pwmSetClock(self.PWM_CLOCK)
        wiringpi.pwmSetRange(self.PWM_RANGE)
        self.logger = lg.get_interface()


    def __str__(self):
        """
        Shows some information about this Servo.
        
        returns: string, Servo info
        """
        
        message = "|Servo status:"
        message += " gpio pin " + str(self.pwm_pin) + "|"
        return message 


    def set_angle(self, angle):
        """
        Sets the servo position to the given angle in degrees.
        
        param: angle, an angle value from 0 to 180 degrees
        """
        
        int_angle = int(angle)
        if not (self.MIN_ANGLE <= int_angle <= self.MAX_ANGLE):
            raise ValueError("Angle value out of range", angle)
        pulse = int(self._get_pulse_width(int_angle))
        if not (self.MIN_PULSE <= pulse <= self.MAX_PULSE):
            raise ValueError("Pulse width out of range", pulse)
        self._position_by_pulse(pulse)
        self.logger.log_message("Servo angle: %d" % angle)


    def set_speed(self, speed):
        """
        Sets the movement speed of the servo arm.
        
        param: speed, a value between 1 and 10
        """
        
        if not (self.MIN_SPEED <= speed <= self.MAX_SPEED):
            raise ValueError("Speed value out of range", speed)
        new_delay = self._get_delay_from_speed(speed)
        self._set_delay(new_delay)
        lm = "Servo speed, delay: %d, %f" % (speed, new_delay)
        self.logger.log_message(lm)


    def drive_open(self):
        """
        Drives the servo to the fully open position.
        """
        
        for pulse in range(self.MIN_PULSE, self.MAX_PULSE, 1):
            wiringpi.pwmWrite(
                self.pwm_pin, 
                self.MAX_PULSE
            )
            ## finish looping at a decent speed
            time.sleep(.01)
        self.curr_pw = self.MAX_PULSE
        self.logger.log_message("Servo driven to open limit")


    def drive_close(self):
        """
        Drives the servo to the fully closed position.
        """
        
        for pulse in range(self.MAX_PULSE, self.MIN_PULSE, -1):
            wiringpi.pwmWrite(
                self.pwm_pin, 
                self.MIN_PULSE
            )
            ## finish looping at a decent speed
            time.sleep(.01)
        self.curr_pw = self.MIN_PULSE
        self.logger.log_message("Servo driven to close limit")


    def _set_delay(self, new_delay):
        """
        Helper method to set the servo's delay setting.
        
        param: new_delay, the delay value to use
        """

        ## if not within delay ranges, throw exception
        if not (
            ## must be close to min threshold or greater than min delay
            (
                abs(self.MIN_DELAY - new_delay) < self.THRESHOLD or 
                new_delay > self.MIN_DELAY
            ) 
                and
            ## must be close to max threshold or less than max delay
            (
                abs(self.MAX_DELAY - new_delay) < self.THRESHOLD or 
                new_delay < self.MAX_DELAY
            )
        ):
            raise ValueError("Delay value not in range", new_delay)
        self.delay = new_delay


    def _position_by_pulse(self, pulse):
        """
        Helper method to set the servo's position by a pulse width.
        The pulse width determines what position the servo is at.
        
        param: pulse, the width between pulses sent by the pwm output
        """
        
        if self.curr_pw < pulse:
            step = 1
        else:
            step = -1
        for p in range(self.curr_pw, pulse, step):
            wiringpi.pwmWrite(
                self.pwm_pin, 
                p
            )
            self.curr_pw = pulse
            time.sleep(self.delay)


    def _get_pulse_width(self, angle):
        """
        Helper method to convert an angle value to a pulse value. This
        allows the servo position to be given as an angle.
                
        param: angle, an angle value from 0 to 180 degrees
        
        returns: float, the pulse width to use as the servo position
        """
        
        return (
            (self.MAX_PULSE - self.MIN_PULSE) / 
            (self.MAX_ANGLE - self.MIN_ANGLE) * 
            angle + self.MIN_PULSE
        )


    def _get_delay_from_speed(self, speed):
        """
        Helper method to convert a speed value to a delay value. This
        allows the servo movement speed to be changed.
                
        param: speed, a value from 1 to 10
        
        returns: float, the delay to pass to the servo
        """
        
        return (
            (self.MAX_DELAY - self.MIN_DELAY) / 
            (self.MAX_SPEED - self.MIN_SPEED) * 
            speed * (-1) + (self.MAX_DELAY + self.MIN_DELAY)
        )

