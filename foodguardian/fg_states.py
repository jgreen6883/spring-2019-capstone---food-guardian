#!/usr/bin/env python
# coding: utf-8


"""
fg_states.py - This is the implementation of the states used by the Food 
    Guardian.
"""


## makes python 2 division and print act like python 3,
## here for compatibility reasons
from __future__ import division, print_function
import state_machine as fsm
import log as lg
import time 


class IdleState(fsm.State):
    """
    This state runs whenever Food Guardian is waiting for a pet to 
    approach the food bowl. The idle state will set a motion condition 
    when something moves in the camera field of view.
    """
    
    def __init__(self, name, camera):
        """
        Constructor for the IdleState class.
        
        param: name, a name for the State
        """
        
        fsm.State.__init__(self, name)
        self.camera = camera
        self.start = time.time()
        ## default conditions
        self.set_conditions(
            {
                ## camera detects motion
                "motion" : fsm.Condition("motion"),
            }
        )
        self.logger = lg.get_interface()


    def enter(self):
        """
        This is run once when moving into the idle state.
        """
        
        print(self.name, "ENTER")
        self.start = time.time()


    def exit(self):
        """
        This is run once when moving out of the idle state.
        """
        
        print(self.name, "EXIT")


    def action(self):
        """
        This is the main body of the idle state.
        """
        
        self.camera.sense()
        if self.camera.sense_object():
            ## small time delay to be sure something is moving to the 
            ## bowl and not just passing by before we try with id
            if time.time() - self.start > 1:
                self.apply_condition(self.conds["motion"])
                print("==============================================")
                print("MOTION FOUND, SET MOTION CONDITION") 
                self.logger.log_message("IdleState: motion detected")
        else:
            self.start = time.time()


class IdentifyState(fsm.State):
    """
    This state runs whenever Food Guardian is determining whether an 
    image of an animal from the camera is a match to the user supplied
    images of the pet that should eat from the food bowl; conditions are 
    set appropriately once the idenfication process is finished.
    """
    
    def __init__(self, name, camera):
        """
        Constructor for the IdentifyState class.
        
        param: name, a name for the State
        """
        
        fsm.State.__init__(self, name)
        self.camera = camera
        ## default conditions
        self.set_conditions(
            {
                ## camera determines pet is a match
                "match" : fsm.Condition("match"),
                ## camera determines pet isn't a match
                "not_match" : fsm.Condition("not_match"),
            }
        )
        self.logger = lg.get_interface()


    def enter(self):
        """
        This is run once when moving into the identify state.
        """
        
        print(self.name, "ENTER")


    def exit(self):
        """
        This is run once when moving out of the identify state.
        """
        
        print(self.name, "EXIT")


    def action(self):
        """
        This is the main body of the identify state.
        """
        
        self.camera.identify()
        match = self.camera.identify_match()
        if match:
            print("MATCH IMAGE, SET MATCH CONDITION")
            self.logger.log_message("IdentifyState: match image")
            self.apply_condition(self.conds["match"])
        else:
            print("NOT MATCH IMAGE, SET NOT MATCH CONDITION")
            self.logger.log_message("IdentifyState: no match")
            self.apply_condition(self.conds["not_match"])


class AccessState(fsm.State):
    """
    This state runs whenever Food Guardian has made a positive match of
    a pet to the user supplied images of the pet that should eat from 
    the food bowl. Conditions are set when the pet is done eating.
    """
    
    def __init__(self, name, camera, lid, access_timeout=45):
        """
        Constructor for the AccessState class.
        
        param: name, a name for the State
        """
        
        fsm.State.__init__(self, name)
        self.camera = camera
        self.lid = lid
        self.access_timeout = access_timeout
        self.start = time.time()
        ## default conditions
        self.set_conditions(
            {
                ## lid has been open long enough to eat
                "timeout" : fsm.Condition("timeout"),
                ## pet finished eating and is gone 
                "not_eating" : fsm.Condition("not_eating"),
            }
        )
        self.logger = lg.get_interface()


    def enter(self):
        """
        This is run once when moving into the access state.
        """
        
        print(self.name, "ENTER")
        self.start = time.time()


    def exit(self):
        """
        This is run once when moving out of the access state.
        """
        
        print(self.name, "EXIT")


    def action(self):
        """
        This is the main body of the access state.
        """
        
        if self.lid.is_closed():
            self.lid.open()
        self.camera.sense()
        pet_eating = self.camera.sense_object()
        over_timeout = time.time() - self.start > self.access_timeout
        condition = None
        if not pet_eating:
            print("PET NOT DETECTED ANYMORE, SET NOT EATING CONDITION")
            self.logger.log_message("AccessState: pet not eating")
            condition = "not_eating"
        elif over_timeout:
            print("ACCESS TIMEOUT, SET TIMEOUT CONDITION")
            self.logger.log_message("AccessState: access timeout")
            condition = "timeout"
        if condition != None:
            self.lid.close()
            self.camera.create_reference_frame()
            self.apply_condition(self.conds[condition])

