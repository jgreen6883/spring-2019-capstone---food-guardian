#!/usr/bin/env python
# coding: utf-8


"""
lid.py - A class to control the lid covering the food bowl.
"""


## makes python 2 division and print act like python 3,
## here for compatibility reasons
from __future__ import division, print_function
import log as lg


class Lid(object):
    """
    This class represents the lid of a food bowl. Food Guardian will
    work the lid to allow a pet to eat or keep other pets from eating.
    """

    def __init__(self, open_angle=90):
        """
        Constructor for the Lid class.
        
        No required parameters to instantiate, but the actuator is 
        not created here. Use the provided method to set the actuator
        later.
        """
        
        self.actuator = None
        self.locked = False
        self.closed = True
        self.open_angle = open_angle
        self.logger = lg.get_interface()
    
    
    def __str__(self):
        """
        Shows some information about this Lid.
        
        returns: string, Lid info
        """
        
        message = "|Lid:"
        if self.is_locked():
            message += " locked,"
        else:
            message += " not locked,"
        message += " " + str(self.open_angle) + " degree angle|"
        return message 


    def open(self):
        """
        Opens the lid to allow food access. Won't open if the lid is 
        locked.
        """
        
        if self.actuator is not None and not self.is_locked():
            try:
                self.actuator.set_angle(self.open_angle)
                self.closed = False
                self.logger.log_message("Lid set to open")
            except ValueError as e:
                print("Invalid angle value", e)


    def close(self):
        """
        Closes the lid to deny food access.
        """
        
        if self.actuator is not None:
            self.actuator.set_angle(0)
            self.closed = True
            self.logger.log_message("Lid set to closed.")


    def is_closed(self):
        """
        Returns whether the lid is closed or not.
        
        returns: boolean, whether the lid is closed or not
        """
        
        return self.closed


    def is_locked(self):
        """
        Returns whether the lid is locked or not.
        
        returns: boolean, whether the lid is locked or not
        """
        
        return self.locked


    def set_speed(self, speed):
        """
        Provides a way to set how fast the lid will raise.
        
        param: speed, a value from 1 to 10
        """
        
        if self.actuator is not None:
            try:
                self.actuator.set_speed(speed)
                self.logger.log_message("Lid speed changed to %d" % speed)
            except ValueError as e:
                print("Invalid Lid speed value", e)


    def set_actuator(self, act):
        """
        Provides a way to set the lid actuator.
        
        param: act, an actuator for the lid to manage
        """
        
        self.actuator = act
        try:
            self.actuator.drive_close()
            self.closed = True
            self.logger.log_message("Lid actuator set to %s" % act.__str__())
        except AttributeError as e:
            self.actuator = None
            print("Invalid Lid actuator", e)


    def set_locked(self, lock):
        """
        Provides a way to lock the lid closed. 
        
        param: lock, a boolean to set the lid lock
        """
        
        self.locked = lock
        if lock == True:
            self.logger.log_message("Lid locked.")
        elif lock == False:
            self.logger.log_message("Lid unlocked.")
        else:
            self.logger.log_message("Lock value not valid: %s" % str(lock))

