#!/usr/bin/env python
# coding: utf-8


"""
main.py - Creates everything needed for the system to work. Run this
	to start.
"""


## makes python 2 division and print act like python 3,
## here for compatibility reasons
from __future__ import division, print_function
import food_guardian as fg
import lid as ld
import camera as cm
import servo_control as sc
import log as lg
import util as ut
import cv2
import argparse
import os
import os.path
import sys


def get_images_paths(path):
    """
    Returns a list of paths to photos in the 'images' directory. These
    are the user's images of the pet that should be able to eat from the 
    food bowl. The supported photo filetypes are '.jpg', '.jpeg', and 
    .png'.
    
    param: a path to a directory of images
    returns: list, a list of paths to photos
    """
    
    photos = []
    for filename in os.listdir(path):
        name, ext = os.path.splitext(filename)
        if ext in SUPPORTED_FORMATS:
            photos.append(os.path.join(IMAGES_DIR, name + ext))
    return photos


## constants for fg config
PWM0_PIN = 18
ACCESS_TIMEOUT = 10
LID_ANGLE = 110
LID_SPEED = 7
MATCH_THRESHOLD = 0.8
## 'images' directory is above the executable path
IMAGES_DIR = ut.get_root_dir("images", sys.argv[0])
SUPPORTED_FORMATS = (".jpg", ".jpeg", ".png")


## read command line arguments
ap = argparse.ArgumentParser()
ap.add_argument(
    "-sf", "--show_frames", 
    action = "store_true", 
    help = "show camera frames on desktop"
)
args = ap.parse_args()


def main():
    """
    Setup Food Guardian operation, then execute Food Guardian logic.
    """
    
    logger = lg.get_interface()
    ## get paths to saved images
    show_frames = args.show_frames
    photos = get_images_paths(IMAGES_DIR)
    if len(photos) <= 0:
        logger.log_message("No user photos available.")
        logger.write_message()
        return
    ## create objects
    camera = cm.Camera(photos, MATCH_THRESHOLD)
    camera.set_show_frames(show_frames)
    servo = sc.ServoControl(PWM0_PIN)
    lid = ld.Lid(LID_ANGLE)
    lid.set_actuator(servo)
    lid.set_speed(LID_SPEED)
    foodguardian = fg.FoodGuardian(camera, lid, ACCESS_TIMEOUT)
    ## execute state logic
    while True:
        logger.write_message()
        foodguardian.execute()
        if show_frames:
            key = cv2.waitKey(1) & 0xFF
            ## if the `q` key is pressed, break from the loop
            ## and close all windows
            if key == ord("q"):
                camera.vs.stop()
                cv2.destroyAllWindows()
                break


if __name__ == "__main__":
    main()

