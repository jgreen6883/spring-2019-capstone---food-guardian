#!/usr/bin/env python
# coding: utf-8


"""
food_guardian.py - The main class. This controls the behavior of the 
    entire system.
"""


## makes python 2 division and print act like python 3,
## here for compatibility reasons
from __future__ import division, print_function
import state_machine as fsm
import fg_states as st
import log as lg


class FoodGuardian(object):
    """
    The Food Guardian utilizes image recognition to allow a particular 
    pet to eat from a food bowl and deny other pets from eating from the 
    same bowl. A set of user-supplied images is used to determine if a
    pet will be allowed to eat or not.
    """

    def __init__(self, camera, lid, access_timeout=45):
        """
        Constructor for the FoodGuardian class. 

        None of the other object instances are made here. Use the setter 
        methods to set them. An optional timeout for bowl access can be 
        given.

        param: camera, a Camera object
        param: lid, a Lid object with actuator set
        param: access_timeout, a maximum time in seconds to keep the lid
            open for the pet to eat
        """
        
        ## this needs to be done before the controller
        self.logger = lg.get_interface()
        self.controller = self._create_controller(
            camera, 
            lid, 
            access_timeout
        )


    def __str__(self):
        """
        Shows some information about this FoodGuardian instance.
        
        returns: string, FoodGuardian info
        """
        curr_state = self.controller.get_state()
        message = "FG status: "
        message += "state is %s," % curr_state.get_name()
        return message 


    def execute(self):
        """
        The main routine. Calls the state controller to execute the action 
        for the current state.
        """

        if self.controller != None:
            current_state = self.controller.get_state()
            try:
                current_state.action()
            except fsm.StateException as e:
                print("Error executing state.action().", e)


    def _create_controller(self, cam, lid, t_out):
        """
        Creates the state machine and a set of needed states for the 
        Food Guardian. The three states created are idle, identify, and 
        access. 
        
        Each state has its own Conditions that are set depending on what 
        actions are taken; these are used by the state machine to decide 
        when to change states. 
        
        When a Condition is applied, a lookup is done to find a matching 
        Transition; this will contain the proper state to move into.
        
        param: cam, a Camera object
        param: lid, a Lid object with actuator set
        param: t_out, an integer value for food access length
        returns: StateMachine, with the states needed to function
        """
        
        fgsm = fsm.StateMachine()
        idle = st.IdleState("idle", cam)
        identify = st.IdentifyState("identify", cam)
        access = st.AccessState("access", cam, lid, t_out)
        idle.set_conditions_call(fgsm.apply_condition)
        identify.set_conditions_call(fgsm.apply_condition)
        access.set_conditions_call(fgsm.apply_condition)
        idle_id = fsm.Transition(idle, idle.get_conditions()["motion"], identify)
        id_eat = fsm.Transition(identify, identify.get_conditions()["match"], access)
        id_idle = fsm.Transition(identify, identify.get_conditions()["not_match"], idle)
        eat_idle_tm = fsm.Transition(access, access.get_conditions()["timeout"], idle)
        eat_no_pet = fsm.Transition(access, access.get_conditions()["not_eating"], idle)
        fgsm.set_transitions([idle_id, id_eat, id_idle, eat_idle_tm, eat_no_pet])
        fgsm.set_state(idle)
        self.logger.log_message("State controller created: %s" % fgsm.__str__())
        return fgsm

