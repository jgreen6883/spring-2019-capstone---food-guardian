#!/usr/bin/env python
# coding: utf-8


"""
camera.py - A class to control the camera monitoring the food bowl.
"""


## makes python 2 division and print act like python 3,
## here for compatibility reasons
from __future__ import division, print_function
## stuff for the camera to work
from imutils.video import VideoStream
import imutils
import cv2
import time
import image_comparison as ic
import log as lg


class Camera(object):
    """
    This class controls the camera. The camera will take and compare 
    images of pets to decide which pets eat from the food bowl.
    """

    WARMUP_SECONDS = 2
    FRAME_THRESH = 50
    FRAME_THRESH_VAL = 255
    C_MIN_AREA = 500


    def __init__(self, images, match_thresh=0.5):
        """
        Constructor for the Camera class.
        
        The only required parameter is a list containing file paths
        to images to be used for image analysis, namely pics of the 
        pet that should be allowd to eat.
        
        param: images, list of pet images
        param: match_thresh, threshold to break for system to decide
            a pet is a match to the pet images
        """
        
        self.images = [self.process_image(path) for path in images]
        self.match_thresh = match_thresh
        self.show_frames = False
        self.obj_sensed = False
        self.id_match = False
        self.vs = None
        self.ref_frame = None
        self.comparison = ic.ImageComparison(self.images)
        ## this needs to be done before init_state()
        self.logger = lg.get_interface()
        self.initialize_state()


    def __str__(self):
        """
		Shows some information about this Camera.
		
		returns: string, Camera info
		"""
        
        message = "Camera state: "
        message += "%d images, " % len(self.images)
        if self.obj_sensed:
            message += "is "
        else:
            message += "no "
        message += "motion, "
        if self.id_match:
            message += "have "
        else:
            message += "no "
        message += "match to saved images "
        return message


    def initialize_state(self):
        """
        This is used to set all class variables to an initial state. 
        It effectively resets the camera.
        """
        
        self.obj_sensed = False
        self.id_match = False
        try:
            self.vs.stop()
        except AttributeError:
            print("Currently no stream to stop().")
            self.vs = None
        self.vs = VideoStream(src=0).start()
        time.sleep(self.WARMUP_SECONDS)
        self.create_reference_frame()
        self.logger.log_message("Camera ready to start")


    def set_show_frames(self, show):
        """
        This will cause the camera to show the current frame, the frame 
        diff, and the frame threshold on the desktop. This requires a 
        window manager to be running before the program launches so the 
        windows can be created.
        
        param: show, a boolean to show the camera frames onscreen
        """
        
        self.show_frames = show 


    def create_reference_frame(self):
        """
        This will create a reference frame used by the camera to detect 
        motion.
        """
        
        frame = self.get_current_frame()
        self.ref_frame = self._process_sense_frame(frame)
        self.logger.log_message("Camera: new reference frame set")


    def get_current_frame(self):
        """
        Pulls the next frame from the camera. The frame info is stored 
        in a numpy array.
        
        returns: ndarray, array of pixel info from the video frame
        """
        
        f = self.vs.read()
        return self._process_input(f)


    def process_image(self, image_path):
        """
        Changes image properties of imported user images.
        
        1) image is resized
        
        return: ndarray, array of pixel info from the image
        """
        
        image = cv2.imread(image_path)
        return self._process_input(image)


    def sense(self):
        """
        Detects motion in the current video frame by comparing it to 
        a reference frame.
        """
        
        ## grab current frame
        f = self.get_current_frame()
        if f is None:
            return
        ## measure the difference of the current frame from the 
        ## reference frame
        f_gray = self._process_sense_frame(f)
        f_delta = cv2.absdiff(self.ref_frame, f_gray)
        _, f_thresh = cv2.threshold(
            f_delta,
            self.FRAME_THRESH,
            self.FRAME_THRESH_VAL,
            cv2.THRESH_BINARY
        )
        ## increase white region of image slighly
        f_thresh = cv2.dilate(
            f_thresh,
            None,
            iterations=2
        )
        ## contours are curves around the boundary of a group of points 
        ## with the same intensity
        contours = cv2.findContours(
            f_thresh.copy(),
            cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE
        )
        contours = imutils.grab_contours(contours)
        self.obj_sensed = False
        for cnt in contours:
            ## a big enough contour means we probably found some object
            ## we care about, small contours are ignored
            if cv2.contourArea(cnt) < self.C_MIN_AREA:
                continue
            self.obj_sensed = True
            ## bound the contour so it will showup in the window in a 
            ## rectangle if the frames are being shown on the desktop
            (x, y, w, h) = cv2.boundingRect(cnt)
            cv2.rectangle(f, (x, y), (x + w, y + h), (0, 255, 0), 2)
        if self.show_frames:
            cv2.imshow("Camera Frame", f)
            cv2.imshow("Frame Thresh", f_thresh)
            cv2.imshow("Frame Delta", f_delta)


    def sense_object(self):
        """
        Returns whether an object is seen moving by the camera.
        
        returns: boolean, whether something is moving in the 
            field of view.
        """
        
        return self.obj_sensed


    def identify(self):
        """
        Compares a frame from the camera to the saved images from the 
        user.
        """
        
        frame = self.get_current_frame()
        self.id_match = self._compare_image(frame)


    def identify_match(self):
        """
        Returns whether an object matches the saved pet images.
        
        returns: boolean, whether an image match has occured
        """
        
        return self.id_match


    def _compare_image(self, image):
        """
        Returns whether a given image matches the saved user images.
        
        param: image, a processed image from the camera
        returns: boolean, whether the image matches the saved images
        """
        
        match = self.comparison.compare_image(image)
        return match >= self.match_thresh


    def _process_input(self, image):
        """
        Changes properties of image input.
        
        1) image is resized
        
        returns: ndarray, array of pixel info from the image
        """
        
        resize = imutils.resize(image, width=self.C_MIN_AREA)
        return resize


    def _process_sense_frame(self, frame):
        """
        Changes properties of the given video frame for motion
        detection.
        
        1) frame is converted to grayscale 
        2) some blurring is applied to smooth out objects
        
        param: frame, a video frame to process
        returns: ndarray, array of pixel info from the image
        """
        
        grayscale = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(grayscale, (21, 21), 0)
        return blurred

