#!/usr/bin/env python
# coding: utf-8


"""
util.py - A module containg functions used throughout the project.
"""


## makes python 2 division and print act like python 3,
## here for compatibility reasons
from __future__ import division, print_function
import os
import os.path


def get_root_dir(dir_name, name=__file__):
    """
    Returns a path to the given directory name. This is used to get the 
    directory paths in the root project directory. If no filename is 
    passed, this script's name is used and it's assumed we're in the same
    directory with main.py when this is called.
    
    param: dir_name, the name of a directory in the project root
    param: name, optional filename of the script calling this function
    returns: string, an absolute path to the given directory name
    """
    
    script_dir= os.path.split(os.path.realpath(name))[0]
    one_up = os.path.split(script_dir)[0]
    return os.path.join(one_up, dir_name)
