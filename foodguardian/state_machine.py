#!/usr/bin/env python
# coding: utf-8


"""
state_machine.py - Has classes needed to implement a finite state
    machine. 
"""


## makes python 2 division and print act like python 3,
## here for compatibility reasons
from __future__ import division, print_function


class StateMachine(object):
    """
    Implementation of a state machine. It uses a list of Transitions and 
    Conditions to determine when to change states.
    """
    
    def __init__(self):
        """
        Constructor for the StateMachine class.
        
        No input needed, but the current state and transitions need to be 
        set before the fsm will actually work. Use the setter methods to 
        add these.
        """
        
        self.current_state = None
        self.transitions = None


    def get_state(self):
        """
        Returns the current state object.
        
        returns: State, the current State object
        """
        
        return self.current_state


    def set_state(self, state):
        """
        Provides a way to set the current state.
        
        param: State, a State object
        """
        
        try:
            self._change_state(state)
        except StateException as e:
            print("Error changing to new state: %s" % state.get_name(), e)


    def set_transitions(self, t):
        """
        Provides a way to set the transitions to change states.
        
        param: t, a list of Transitions
        """
        
        self.transitions = t


    def add_transition(self, t):
        """
        Adds a transition to the state.
        
        param: t, a new Transition for the state
        """
        
        self.transitions.append(t)


    def apply_condition(self, condition):
        """
        Compares the given condition with the stored transitions to 
        determine the state to change to.
        
        param: condition, a Condition matching some state change
        """
        
        try:
            going = self._get_next_state(condition)
        except StateException as e:
            print("Error applying state condition.", condition, e)
        else:
            self.set_state(going)


    def _get_next_state(self, condition):
        """
        Helper method for finding the next state. Looks through all the 
        Transitions for the matching Condition.
        
        param: condition, a Condition matching some state change
        returns: State, the new State to change to
        raises: StateException, Condition doesn't match with any of the 
            stored Transitions
        """
        
        if self.transitions == None:
            raise StateException("No transitions available.")
        for trans in self.transitions:
            from_match = trans.from_ == self.current_state
            conds_match = trans.condition == condition
            if from_match and conds_match:
                return trans.to
        raise StateException("No matching state to change to.")


    def _change_state(self, going):
        """
        Helper method to change the current state to a new state. It will
        run the enter() and exit() methods for the previous and next 
        states.
        
        param: going, a new State object to change into
        raises: StateException, error running the exit() and enter()
            methods from the previous and next states.
        """
        
        leaving = self.get_state()
        self.current_state = going
        if leaving != None:
            try:
                leaving.exit()
            except AttributeError as e:
                raise StateException("Error leaving current state.", e) from None
        try:
            going.enter()
        except AttributeError as e:
            raise StateException("Error going to new state.", e) from None


class State(object):
    """
    This class models the state of an object. The state can set a condition
    based as needed to flag when things are happening. The conditions can
    be used to determine when to change from state to state.
    """
    
    def __init__(self, name):
        """
        Constructor for the State class. 
        
        The state needs a name and a way to set conditions as they happen 
        so the parent controller can set the correct state based on them.
        Use the setter method to set the conditions callback.
        
        param: name, the name of the State
        """
        
        self.name = name
        self.apply_cond = None
        self.conds = {}


    def __str__(self):
        """
        Returns a string representing the object.
        
        returns: string, object information
        """
        
        return str(self.name)


    def get_name(self):
        """
        Returns the name of the state.
        
        returns: string, name of state
        """
        return self.name


    def get_conditions(self):
        """
        Returns the conditions that can be set by this state.
        
        returns: dict, a dictionary of possible conditions
        """
        
        return dict(self.conds)


    def set_conditions(self, conds):
        """
        Provides a way to set the conditions set by this state.
        
        param: conds, a dictionary of Conditions
        """
        
        self.conds = conds


    def add_condition(self, cond):
        """
        Adds a conditions to this state.
        
        param: cond, a new Condition for the state.
        """
        
        self.conds.append(cond)


    def set_conditions_call(self, apply_cond):
        """
        Provides a way to set the callback for the state conditions.
        
        param: apply_conds, a callback for state Conditions
        """
        
        self.apply_cond = apply_cond


    def apply_condition(self, condition):
        """
        Passes a set of active conditions to the controlling state 
        machine. 
        
        param: condition, a Condition given from a state
        """
        
        self.apply_cond(condition)


    def enter(self):
        """
        This is called once when moving into the state. Each state
        must implement this method.
        
        raises: NotImplementedError
        """
        
        raise NotImplementedError


    def exit(self):
        """
        This is called once when exiting the state. Each state must 
        implement this method.
        
        raises: NotImplementedError
        """
        
        raise NotImplementedError


    def action(self):
        """
        This is the main action performed while in the state. Each state
        must implement this method.
        
        raises: NotImplementedError
        """
        
        raise NotImplementedError


class Transition(object):
    """
    A transition is used to keep track of what combinations of state and 
    conditions cause a change to another state.
    """
    
    def __init__(self, from_state, condition, to_state):
        """
        Constructor for the Transition class.
        
        This keeps track of the combinations of current state and 
        conditions that make an object move to another state.
        
        param: from_state, a State to move out of
        param: condition, a Condition to cause the state change
        param: to_state, a new State to move to
        """
        
        self.from_ = from_state
        self.condition = condition
        self.to = to_state


class Condition(object):
    """
    This class represents a condition. These are used to determine what 
    state we need to be in.
    """
    
    def __init__(self, name):
        """
        Constructor for the Condition class.
        
        param: name, the name of the condition
        """
        
        self.name = name
    
    
    def __str__(self):
        """
        Returns a string representing the object.
        
        returns: string, object information
        """
        
        return str(self.name)


class StateException(Exception):
    """
    An exception to throw for errors in the StateMachine.
    """
    
    def __init__(self, message, *args):
        """
        Constructor for the StateException class.
        
        param: message, an error message
        """
        
        Exception.__init__(self, message, *args)
        self.message = message


"""
can use a set of states for from and to states in Transition and a 
set of conditions to help with composite states of objects
Transition(set(from_states) from, set(conditions) conds, set(to_states) to)
StateMachine.apply_conditions() would check for equality of the sets
to change to new states
"""

