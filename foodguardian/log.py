#!/usr/bin/env python
# coding: utf-8


"""
log.py - Manages a log of system messages.
"""


## makes python 2 division and print act like python 3,
## here for compatibility reasons
from __future__ import division, print_function
import os
import os.path
import util as ut


class InterfaceLog(object):
    """
    Provides access to the system log events.
    """
    
    class Log(object):
        """
        Stores system events in local files.
        """
        
        CURR = "log.current"
        LAST = "log.last"
        LOG_DIR = ut.get_root_dir("logs", __file__)

        
        def __init__(self, size=2):
            """
            Constructor for the Log class. 
            
            param: size, optional size on disk in megabytes
            """
            
            self.current = os.path.join(self.LOG_DIR, self.CURR)
            self.last = os.path.join(self.LOG_DIR, self.LAST)
            ## filesize on disk in MB
            self.size = size * 1000000
            ## TODO: use a deque instead of a list
            self.mq = []


        def get_log(self):
            """
            Returns the contents of the log in a list, one element per
            line.
            
            returns: list, contains logfile lines
            """
            
            lines = []
            with open(self.last, "r") as f_l:
                for line in f_l:
                    lines.append(line[:-1])
            with open(self.current, "r") as f_c:
                for line in f_c:
                    lines.append(line[:-1])
            return lines


        def set_size(self, size):
            """
            Provides a way to set the max size of the logfile on disk.
            
            param: size, the new max size on disk
            """
            
            self.size = size


        def log_message(self, message):
            """
            Adds a message to the written to the log.
            
            param: message, a string to be written to disk
            """
            
            self.mq.append(str(message))


        def write_message(self):
            """
            Writes the next log message out to disk.
            """
            
            if len(self.mq) > 0:
                with open(self.current, "a") as f:
                    f.write(self.mq.pop(0) + "\n")
                self._rotate()


        def _rotate(self):
            """
            Compares the current log filesize to the max size. Once the 
            log size reaches the max, the oldest log entries are removed, 
            the current log becomes the last log, and a blank file is 
            created to be written to. This way there's at least one whole 
            file of log info available and we don't fill up local device
            storage.
            """
            
            current = self.current
            last = self.last
            size_on_disk = os.path.getsize(current)
            if size_on_disk > self.size:
                os.remove(last)
                os.rename(current, last)
                open(current, "w").close()
        
        
    ## single instance of log for all interface instances
    _log = None
        
        
    def __init__(self):
        """
        Constructor for the InterfaceLog class. 
        """
        
        if InterfaceLog._log == None:
            InterfaceLog._log = InterfaceLog.Log()
        self.__dict__["_log"] = InterfaceLog._log
    
    
    def get_log(self):
        """
        Returns the contents of the log in a list, one element per
        line.
        
        returns: list, contains logfile lines
        """
        
        return self._log.get_log()


    def set_size(self, size):
        """
        Provides a way to set the max size of the logfile on disk.
        
        param: size, the new max size on disk
        """
        
        self._log.set_size(size)


    def log_message(self, message):
        """
        Adds a message to the written to the log.
        
        param: message, a string to be written to disk
        """
        
        self._log.log_message(message)


    def write_message(self):
        """
        Writes the next log message out to disk.
        """
        
        self._log.write_message()


def get_interface():
    """
    Returns an interface to a singleton Log.
    
    return: InterfaceLog, interface with the Log
    """
    
    return InterfaceLog()

