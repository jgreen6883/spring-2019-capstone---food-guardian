import imutils
import cv2
import matplotlib.pyplot as plt

## paths to saved images on disk
img_paths = [
	"../images/img1.jpg",
	"../images/img2.jpg",
	"../images/img3.jpg"
]

## some test images to compare to saved images
test_paths = [
	"../images/t_img1.jpg",
	## "../images/t_img2.jpg",
	## "../images/t_img3.jpg"
]

## temp storage for histograms
tests = {}
t_images = {}
images = {}
hists = {}
results = {}

## load test hists
for path in test_paths:
    img = cv2.imread(path)
    img = imutils.resize(img, width=500)
    t_images[path] = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    hist = cv2.calcHist(
        [img], 
        [0, 1, 2], 
        None, 
        [8, 8, 8], 
        [0, 256, 0, 256, 0, 256]
    )
    norm_hist = cv2.normalize(hist, hist).flatten()
    tests[path] = norm_hist

## load saved hists
for path in img_paths:
    img = cv2.imread(path)
    img = imutils.resize(img, width=500)
    images[path] = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    hist = cv2.calcHist(
        [img], 
        [0, 1, 2], 
        None, 
        [8, 8, 8], 
        [0, 256, 0, 256, 0, 256]
    )
    norm_hist = cv2.normalize(hist, hist).flatten()
    hists[path] = norm_hist

## comparison methods
METHODS = (
    ("Correlation", cv2.HISTCMP_CORREL),
    ("Chi-Squared", cv2.HISTCMP_CHISQR),
    ("Intersection", cv2.HISTCMP_INTERSECT),
    ("Hellinger", cv2.HISTCMP_BHATTACHARYYA),
)

## do each hist comparison here
for (methodName, method) in METHODS:
    print(methodName)
    results = {}
    reverse = False
    if methodName == "Correlation" or methodName == "Intersection":
        reverse = True
    for path, hist in hists.items():
        dist = cv2.compareHist(tests[test_paths[0]], hist, method)
        results[path] = dist 
        print(dist)
    results = sorted([(v, k) for (k, v) in results.items()], reverse=reverse)
    print(results)
    print()
    
    ## show query window
    fig = plt.figure("Query")
    ax = fig.add_subplot(1, 1, 1)
    ax.imshow(t_images[test_paths[0]])
    plt.axis("off")
    ## show each method results
    fig = plt.figure("Results: %s" % (methodName))
    fig.suptitle(methodName, fontsize = 20)
    ## show each image comparison per method
    for (i, (v, k)) in enumerate(results):
        ax = fig.add_subplot(1, len(images), i + 1)
        ax.set_title("%s: %.2f" % (k, v), fontsize=10)
        plt.imshow(images[k])
        plt.axis("off")

## actually show all the windows
plt.show()
