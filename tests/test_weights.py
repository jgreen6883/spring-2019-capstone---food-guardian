import imutils
import cv2
import matplotlib.pyplot as plt

## paths to saved images on disk
img_paths = [
	"../images/img1.jpg",
	"../images/img2.jpg",
	## "../images/img3.jpg"
]

## some test images to compare to saved images
test_paths = [
	"../images/t_img1.jpg",
	## "../images/t_img2.jpg",
	## "../images/t_img3.jpg"
]

## temp storage for histograms
tests = {}
t_images = {}
images = {}
hists = {}
results = {}

## store weights for methods
M_WEIGHTS = {
    "Correlation" : 0.4,
    "Chi-Squared" : 0.1,
    "Intersection" : 0.4,
    "Hellinger" : 0.1,
}
N_WEIGHTS = {
    "Correlation" : 0.1,
    "Chi-Squared" : 0.4,
    "Intersection" : 0.1,
    "Hellinger" : 0.4,
}
method_results = {
    "Correlation" : 0,
    "Chi-Squared" : 0,
    "Intersection" : 0,
    "Hellinger" : 0,
}

def iVal(val):
    return val/4.02

def hVal(val):
    return -val + 1

def csVal(val):
    return (val/1728.0) + 1

## load test hists
for path in test_paths:
    img = cv2.imread(path)
    img = imutils.resize(img, width=500)
    t_images[path] = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    hist = cv2.calcHist(
        [img], 
        [0, 1, 2], 
        None, 
        [8, 8, 8], 
        [0, 256, 0, 256, 0, 256]
    )
    norm_hist = cv2.normalize(hist, hist).flatten()
    tests[path] = norm_hist

## load saved hists
for path in img_paths:
    img = cv2.imread(path)
    img = imutils.resize(img, width=500)
    images[path] = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    hist = cv2.calcHist(
        [img], 
        [0, 1, 2], 
        None, 
        [8, 8, 8], 
        [0, 256, 0, 256, 0, 256]
    )
    norm_hist = cv2.normalize(hist, hist).flatten()
    hists[path] = norm_hist

## comparison methods
METHODS = (
    ("Correlation", cv2.HISTCMP_CORREL),
    ("Chi-Squared", cv2.HISTCMP_CHISQR),
    ("Intersection", cv2.HISTCMP_INTERSECT),
    ("Hellinger", cv2.HISTCMP_BHATTACHARYYA),
)

## do each hist comparison here
for (methodName, method) in METHODS:
    d = 0.0
    print(methodName)
    results = {}
    reverse = False
    if methodName == "Correlation" or methodName == "Intersection":
        reverse = True
    for path, hist in hists.items():
        dist = cv2.compareHist(tests[test_paths[0]], hist, method)
        d += dist
        results[path] = dist 
        print(dist)
    method_results[methodName] = d / len(hists)
    if methodName == "Intersection":
        method_results["Intersection"] = iVal(method_results["Intersection"])
    if methodName == "Hellinger":
        method_results["Hellinger"] = hVal(method_results["Hellinger"])
    if methodName == "Chi-Squared":
        method_results["Chi-Squared"] = csVal(method_results["Chi-Squared"])
    results = sorted([(v, k) for (k, v) in results.items()], reverse=reverse)
    print(results)
    print()
    
    ## show query window
    fig = plt.figure("Query")
    ax = fig.add_subplot(1, 1, 1)
    ax.imshow(t_images[test_paths[0]])
    plt.axis("off")
    ## show each method results
    fig = plt.figure("Results: %s" % (methodName))
    fig.suptitle(methodName, fontsize = 20)
    ## show each image comparison per method
    for (i, (v, k)) in enumerate(results):
        ax = fig.add_subplot(1, len(images), i + 1)
        ax.set_title("%s: %.2f" % (k, v), fontsize=10)
        plt.imshow(images[k])
        plt.axis("off")

print("\nMethod Results:")
print(method_results, "\n")
mval, nval = 0, 0
for k, v in method_results.items():
    tmval, tnval = 0, 0
    tmval += method_results[k] * M_WEIGHTS[k]
    tnval += method_results[k] * N_WEIGHTS[k]
    print("MVAL", k, tmval)
    print("NVAL", k, tnval)
    mval += tmval
    nval += tnval
print("MATCH:", str(mval), " --", "NOT-MATCH:", str(nval))
print("PET IMAGE MATCH:", (mval - nval), (mval - nval) > 0)
## actually show all the windows
plt.show()
