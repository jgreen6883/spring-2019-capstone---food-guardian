import camera as cam
from imutils.video import VideoStream
import imutils
import cv2
import time, random


start = time.time()
now = start
id_duration = 2
eat_duration = 4
count = 0
states = ("idle", "identify", "access")
curr_state = "idle"


photos = [
	"../images/img1.jpg",
	"../images/img2.jpg",
	"../images/img3.jpg"
]


r = cam.Camera(photos)


"""
while True:
	r.sense()
	if r.sense_object():
		print("CHANGE TO ID STATE", count)
		count += 1
		r.create_reference_frame()
	key = cv2.waitKey(1) & 0xFF
	## if the `q` key is pressed, break from the loop
	if key == ord("q"):
		r.vs.stop()
		cv2.destroyAllWindows()
		break
"""


"""
print("START IN IDLE STATE, WAITING FOR MOTION")
print()
while True:
	##  todo in idle state
	if curr_state == "idle":
		## tell the camera to look for motion
		r.sense()
		## see if it saw anything
		if r.sense_object():
			count += 1
			print("MOTION FOUND, CHANGE TO IDENTIFY STATE", count)
			## swap to identify state to check an image of whatever
			## caused the motion
			curr_state = "identify"			
			start = time.time()
	## todo in identify state
	elif curr_state == "identify":
		## take snapshot from camera and compare to saved image(s)
		now = time.time()
		if now - start > id_duration:
			match = random.choice([True, False])
			## check match value
			if match:
				## close enough match, switch to food access state
				print("MATCH IMAGE, CHANGE TO ACCESS STATE", count)
				curr_state = "access"
				start = time.time()
			else:
				## not enough of a match, switch back to idle state
				print("NOT MATCH IMAGE, CHANGE TO IDLE STATE", count)
				## recreate reference frame for next motion detection 
				curr_state = "idle"
				r.create_reference_frame()
				print()
	## todo in food access state
	elif curr_state == "access":
		## check to see if the pet is still eating
		now = time.time()
		if now - start > eat_duration:
			## not eating, so do stuff here to reset to idle state
			print("PET NOT DETECTED ANYMORE, CHANGE TO IDLE STATE", count)
			## recreate reference frame for next motion detection 
			curr_state = "idle"
			r.create_reference_frame()
			print()
	key = cv2.waitKey(1) & 0xFF
	## if the `q` key is pressed, break from the loop
	if key == ord("q"):
		r.vs.stop()
		cv2.destroyAllWindows()
		break
"""
