import servo_control as sc


## create an object to interact with servo
s = sc.ServoControl(18)
s.set_speed(9)


## start by driving the servo completely closed
s.drive_close()

	
## move it to a series of angle positions
s.set_angle(180)
s.set_angle(0)
s.set_angle(150.345)
s.set_angle(45)
s.set_angle(90)
s.set_angle(0)

