import food_guardian as fg
import lid as ld
import servo_control as sc
import time 


start = time.clock()
duration = 2
count = 0


## create stuff
f = fg.FoodGuardian()
s = sc.ServoControl(18)
l = ld.Lid(90)


## add stuff
l.set_controller(s)
l.set_speed(10)
f.set_lid(l)
f.set_camera("|'SOME CAMERA OBJECT'|")


## run stuff to demo
while True:
	now = time.clock()
	if now - start > duration:
		f.execute()
		start = now
		count += 1
		count = count % len(f.STATES)
