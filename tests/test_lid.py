import servo_control as sc
import lid as sl
import time 


## create a lid and controller
s = sc.ServoControl(18)
l = sl.Lid(75)


## add control to lid
l.set_controller(s)
l.set_speed(7)

## drive lid open and closed
l.set_locked(False)
while True:
	time.sleep(3)
	l.open()
	time.sleep(3)
	l.close()

