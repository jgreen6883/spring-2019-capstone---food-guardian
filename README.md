Food Guardian

The idea for this was born from a college course I took a few years ago. 
As part of a group, we were assigned the task of creating a hardware solution to the problem
of one pet eating another pet's food from their bowl. The implementation used
a camera along with image detection software to identify a particular 
pet and only allow them to eat from the bowl - others would not be 
allowed access. The bowl was covered by a lid that moved when 
the correct pet approaches the bowl. 

A raspberry pi drove a servo that moved the lid up and down; a pi camera 
was mounted on top to look for approaching pets.
